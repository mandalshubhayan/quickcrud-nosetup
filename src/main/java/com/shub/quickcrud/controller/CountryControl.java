package com.shub.quickcrud.controller;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shub.quickcrud.model.Country;
import com.shub.quickcrud.service.CountrySvc;

@RestController
@RequestMapping("/country" )

public class CountryControl {
	
	private CountrySvc svc;

	public CountryControl(CountrySvc svc) {
		super();
		this.svc = svc;
	}
	
	@GetMapping("/geyCountry")
	public CompletableFuture<Country> getCountry(@RequestParam(required = true) Integer id) throws InterruptedException, ExecutionException {
		
		System.out.println(Thread.currentThread().getName());
		return svc.getName(id);
	}
	
	@PostMapping("/createCountry")
	public Country createCountry(@RequestBody(required = true) Country country) {
		
		
		return svc.createCountry(country);
	}
	

}
