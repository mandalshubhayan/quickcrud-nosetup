package com.shub.quickcrud.doa;

import java.util.concurrent.CompletableFuture;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Repository;

import com.shub.quickcrud.model.Country;


@Repository
@EnableAsync
public interface CountryRepo extends JpaRepository<Country, Integer> {

	@Async
   CompletableFuture<Country> findByName(String name);

	 
	
}
