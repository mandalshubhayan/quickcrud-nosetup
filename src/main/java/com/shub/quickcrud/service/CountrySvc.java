package com.shub.quickcrud.service;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import com.shub.quickcrud.doa.CountryRepo;
import com.shub.quickcrud.model.Country;

@Service
@EnableAsync
public class CountrySvc {

	private CountryRepo countryRepo;

	public CountrySvc(CountryRepo countryRepo) {
		super();
		this.countryRepo = countryRepo;
	}

	public CountryRepo getCountryRepo() {
		return countryRepo;
	}

	public void setCountryRepo(CountryRepo countryRepo) {
		this.countryRepo = countryRepo;
	}
	
	
	@Async
	public CompletableFuture<Country> getName(Integer counId) throws InterruptedException, ExecutionException {
		System.out.println(Thread.currentThread().getName());

		
		return countryRepo.findByName("India");
		 
	}
	
	public Country createCountry(Country country) {
		
		return countryRepo.save(country);
		
		
	}
	
}
