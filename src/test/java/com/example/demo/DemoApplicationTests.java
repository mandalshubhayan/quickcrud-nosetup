package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;

import com.shub.quickcrud.H2CrudApplication;
import com.shub.quickcrud.model.Country;

@SpringBootTest(classes = H2CrudApplication.class,webEnvironment = WebEnvironment.RANDOM_PORT)

class DemoApplicationTests {

	@Test
	void contextLoads() {
	}

	@LocalServerPort
	Integer port;
	
	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	@Sql(scripts = "./sampledata.sql")
	void homeResponse() {
		Country body = this.restTemplate.getForObject("/country/geyCountry?id=1", Country.class, new HashMap<>().put("id",1));
		assertEquals("Spring is here!", body.getName());
	}
}
