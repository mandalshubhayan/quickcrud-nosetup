package com.shub.quickcrud.doa;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;

import com.shub.quickcrud.model.Country;

@DataJpaTest
class CountryRepoTest {


	@Autowired
	CountryRepo countryRepo;
	
	
	
	@Test
	@Sql(scripts = "./sampledata.sql")
	void testFindById() {
		Country country = countryRepo.findById(1).get();
		assertEquals(country.getName(), "null");
		
	}

}
